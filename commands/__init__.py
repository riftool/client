import logging
import uuid
from datetime import datetime

from jsonschema import validate


class BaseCommand:

    def __init__(self, _id, configuration):
        self.configuration = configuration
        self.id = _id
        self.configuration_schema = {}
        self.logger = logging.getLogger(__name__)

    @property
    def name(self):
        return self.__class__.__name__

    def validate(self):
        validate(instance=self.configuration, schema=self.configuration_schema)

    def execute(self):
        self.logger.debug(f'Executing command "{self.name}".')
        self._execute()

    def _execute(self):
        raise NotImplementedError

    def _save(self, is_success, data):
        raise NotImplementedError

    @staticmethod
    def _generate_id():
        return str(uuid.uuid4())

    @staticmethod
    def _get_utc_now():
        return str(datetime.utcnow())
