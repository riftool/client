import json
import subprocess

from commands import *


class ShellCommands(BaseCommand):

    def __init__(self, *args, **kwargs):
        super(ShellCommands, self).__init__(*args, **kwargs)
        self.configuration_schema = {
            'type': 'object',
            'properties': {
                'command': {'type': 'string'},
                'interval': {'type': 'string'},
            },
            'required': ['command']
        }
        self.validate()

    def _execute(self):
        command = self.configuration['command']
        res = subprocess.run(command, shell=True, capture_output=True)
        if res.stderr:
            self._save(False, res.stderr.decode('utf8'))
        self._save(True, res.stdout.decode('utf8'))

    def _save(self, is_success, data):
        res = {
            'is_success': is_success,
            'command_id': self.id,
            'product_id': self._generate_id(),
            'utc_time': self._get_utc_now(),
            'data': data
        }
        with open(self._generate_id(), 'w+') as _file:
            _file.write(json.dumps(res))
